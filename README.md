Ansible role to deploy a simple pf firewall.

The role only take care of inbound tcp port, to be used for really
simple cases (such as a regular server with 1 single interface).

# Example usage

In order to add a firewall, one can just do that:
```
$ cat deploy_pf_firewall.yml
- hosts: freebsd
  roles:
  - role: pf_firewall
    tcp_services:
    - ssh
    - smtp
    udp_services:
    - domain
```

The role take care of starting pf, and reloading the firewall after checking
it for every modification.
